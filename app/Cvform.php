<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cvform extends Model
{
    protected $table = "cvforms";
    protected $fillable = ['name', 'email', 'birth', 'nrc_no', 'certificate', 'university', 'occupation', 'address', 'telephone', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function jobs()
    {
        return $this->belongsToMany('App\Job', 'cvform_job', 'cvform_id', 'job_id')->withPivot('apply_status');
    }
}
