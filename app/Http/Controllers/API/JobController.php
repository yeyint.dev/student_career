<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Job;
use App\Cvform;
use App\User;
use Auth;

class JobController extends Controller
{

    public function storeApplierAndJob(Request $request)
    {
        $job = Job::where('id', $request->job_id)->first();

        $job->appliers()->attach($request->user_id, ['apply_status' => 'applied']);

        return response("Applied");
    }

    public function searchByTitle($search)
    {
        $jobs = Job::where('title', 'like', '%'. $search .'%')
                    ->orWhere('company_name', 'like', '%'. $search .'%')
                    ->get();
        return response()->json($jobs, 200);
    }

    public function searchByLocation($search)
    {
        $jobs = Job::where('location', 'like', '%'. $search .'%')
                    ->get();
        return response()->json($jobs, 200);
    }

    public function userJob($user_id)
    {
        $jobs = Job::where('user_id', $user_id)->paginate(8);

        return response()->json($jobs, 200);
    }

    public function applierLists($job)
    {
        $result = Job::find($job)->appliers()->get();
        // $jobApplierLists = $result->cvform()->get();

        return response()->json($result, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job = Job::orderBy('id', 'desc')->paginate(8);
        return response($job->jsonSerialize(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Job::create($request->all());
        return response(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($job)
    {
        $job = Job::findOrFail($job);

        return response()->json($job, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $job)
    {
        $job = Job::findOrFail($job);
        $job->update($request->all());

        return response(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($job)
    {
        $job = Job::findOrFail($job);
        $job->delete();
        return response(200);
    }
}
