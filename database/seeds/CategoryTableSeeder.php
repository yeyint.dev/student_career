<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'catgory1',
            'description' => 'Hello World',
            'user_id' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'catgory2',
            'description' => 'Hello World',
            'user_id' => 1,
        ]);
        DB::table('categories')->insert([
            'name' => 'catgory3',
            'description' => 'Hello World',
            'user_id' => 1,
        ]);
    }
}
