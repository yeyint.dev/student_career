<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->colorName,
        'description' => $faker->word,
        'user_id' => 1,
    ];
});
