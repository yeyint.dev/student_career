<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->catchPhrase,
        'content' => $faker->paragraph($nbSentences = 3, $variableNbSentences = true),
        'image' => $faker->imageUrl($width = 750, $height = 400),
        'user_id' => factory('App\User')->create()->id,
        'category_id' => factory('App\Category')->create()->id,
    ];
});
